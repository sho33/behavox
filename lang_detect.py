import sys
import re
import nltk
from sklearn.externals.joblib import load


def _trim_string(string):
    # remove extra spaces, remove trailing spaces 
    return re.sub('\s+',' ',string).strip()

def clean_string(string):
    string = _trim_string(string)
    # Add more cleaning if required
    return string

def _replace_period(text):
    return re.sub('。', '. ', text)

def _replace_comma(text):
    return re.sub('、', '', text)

def replace_ja_punctuation(text):
    # replace ja punctuations for sent_tokenize
    text = _replace_period(text)
    text = _replace_comma(text)
    return text 

def proc_text(text):  
    with open(text, encoding='utf-8') as f:
        content = f.read()
        # print(content) 
    content = replace_ja_punctuation(content)    
    sentences = nltk.sent_tokenize(content)
    proc_sentences = [clean_string(sentence) for sentence in sentences]
    return proc_sentences
    # print(sentences)
    # print(proc_sentences)

def read_labels(file_path='models/label_names.txt'):
    # define empty list
    label_names = []
    # open file and read the content in a list
    with open(file_path, 'r') as f:  
        label_names = [label.rstrip() for label in f.readlines()]
    return label_names

def load_model(model_path='models/lang_dect.pkl'):
        return load(model_path) 

def save_results(sents, preds, label_names, file_path='output.txt'):
    with open(file_path, 'w', encoding='utf-8') as f:
        for sentence, lang in zip(sents, preds):
            f.write('<{1}> {0} </{1}>\n'.format(sentence, label_names[lang]))
        f.close()

def main():
    # load
    script = sys.argv[0]
    filename = sys.argv[1]
    label_names = read_labels()
    clf = load_model()
    sentences = proc_text(filename)

    # Predict
    predicted_languages = clf.predict(sentences)
    for sentence, lang in zip(sentences, predicted_languages):
        print('{} ----> {}'.format(sentence, label_names[lang]))

    # save
    save_results(sentences, predicted_languages, label_names)


if __name__ == '__main__':
   main()

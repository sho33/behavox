import os
import re
import sys
import glob
import nltk
import gensim
import numpy as np
import pandas as pd
from tqdm import tqdm
from uuid import uuid4
from functools import reduce
from multiprocessing import Pool
# from pdb import set_trace


def _trim_string(string):
    # remove extra spaces, remove trailing spaces, lower the case 
    return re.sub('\s+',' ',string).strip().lower()

def clean_string(string,
                 min_len=2,
                 max_len=30):

    string = _trim_string(string)
    # remove short words craps
    string = ' '.join(gensim.utils.simple_preprocess(string,
                                                     min_len=min_len,
                                                     max_len=max_len))
    return string

def splitkeepsep(s, sep):
    cleaned = []
    s = re.split("(%s)" % re.escape(sep), s)
    for _ in s:
        if _!='' and _!=sep:
            cleaned.append(sep+_)
    return cleaned

def remove_html_tags(text):
    """Remove html tags from a string"""
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

def _replace_period(text):
    return re.sub('。', '. ', text)

def _replace_comma(text):
    return re.sub('、', '', text)

def replace_ja_punctuation(text):
    # replace ja punctuations for sent_tokenize
    text = _replace_period(text)
    text = _replace_comma(text)
    return text 

def process_wiki_files(wiki_file):
    chars = ['\n']
    
    with open(wiki_file, encoding='utf-8') as f:
        content = f.read()

    articles = splitkeepsep(content,'<doc id=')
    df = pd.DataFrame(columns=['article_uuid','sentence','proc_sentence','proc_len'])
    
    for article in articles:
        uuid = uuid4()
        # set_trace()
        article = replace_ja_punctuation(remove_html_tags(article))

        sentences = nltk.sent_tokenize(article)
        proc_sentences = [clean_string(sentence) for sentence in sentences]
        proc_lens = [len(sentence.split(' ')) for sentence in proc_sentences]
        
        temp_df = pd.DataFrame(
            {'article_uuid': [uuid]*len(sentences),
             'sentence': sentences,
             'proc_sentence':proc_sentences,
             'proc_len':proc_lens
            })
        df = df.append(temp_df) 
    return df

def list_multiprocessing(param_lst,
                         func,
                         **kwargs):
    
    workers = kwargs.pop('workers')

    with Pool(workers) as p:
        apply_lst = [([params], func, i, kwargs) for i,params in enumerate(param_lst)]
        result = list(tqdm(p.imap(_apply_lst, apply_lst), total=len(apply_lst)))

    # lists do not need such sorting, but this can be useful later
    result=sorted(result,key=lambda x:x[0])
    return [_[1] for _ in result]

def _apply_lst(args):
    params, func, num, kwargs = args
    return num, func(*params,**kwargs)


wiki_langs = [f for f in os.listdir("data") if not (f.startswith('.') or f.endswith('.csv'))]

print(wiki_langs)

# CSV for each lang
df_list = []
#wiki_langs = ['ja']
for lang in wiki_langs:
    wiki_files = []
    for filename in glob.iglob('data/' + lang + '/*/*', recursive=True):
        wiki_files.append(filename)
        #wiki_files = wiki_files[:9]
    
    print(lang)
    df = list_multiprocessing(wiki_files,
                              process_wiki_files,
                              workers=4)

    df = pd.concat(df).reset_index(drop=True)
    df.dropna(subset=['proc_sentence'], inplace=True)
    df['lang'] = lang
    # df.to_csv('data/' + lang + '.csv')
    df_list.append(df)

pd.concat(df_list).reset_index(drop=True).to_csv('data/data.csv')






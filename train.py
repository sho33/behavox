
import os
import pandas as pd
import numpy as np
from sklearn import ensemble
from sklearn import feature_extraction
from sklearn import linear_model
from sklearn import pipeline
from sklearn import model_selection
# from sklearn import cross_validation
from sklearn import metrics
from sklearn.preprocessing import LabelEncoder
from sklearn.externals import joblib
# from pdb import set_trace

CURRENT_DIR = os.getcwd()
PROJECT_HOME_DIR = CURRENT_DIR

DATA_DIR = os.path.abspath(os.path.join(PROJECT_HOME_DIR, 'data'))

df = pd.read_csv(DATA_DIR + '/' + 'data.csv')
df.dropna(subset=['proc_sentence'], inplace=True)
print(df.groupby(['lang']).size().reset_index(name='counts'))
# print(df.groupby(['lang']).proc_len.mean().reset_index(name='sents_mean'))
# print(df.groupby(['lang']).proc_len.std().reset_index(name='sents_std'))


# training
le = LabelEncoder()
le.fit(df.lang.unique())
label_names = le.classes_
df['target'] = le.transform(df.lang)


X_train, X_test, y_train, y_test = model_selection.train_test_split(df.proc_sentence,
                                                                     df.target,
                                                                     test_size=0.2,
                                                                     random_state=0)
# character level n-gram as feature
vectorizer = feature_extraction.text.TfidfVectorizer(ngram_range=(1, 3),
                             analyzer='char',)
# pipline
pipe = pipeline.Pipeline([
    ('vectorizer', vectorizer),
    ('clf', linear_model.LogisticRegression())
])

pipe.fit(X_train, y_train)

y_predicted = pipe.predict(X_test)
cm = metrics.confusion_matrix(y_test, y_predicted)
print(metrics.classification_report(y_test, y_predicted,
                                    target_names=label_names))

# save label names
with open('models/label_names.txt', 'w') as f:  
    f.writelines("%s\n" % label for label in label_names)


# save pipline
joblib.dump(pipe, 'models/lang_dect.pkl')


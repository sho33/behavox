---

# Language Detectin Challenge

---

## Usage

The software takes text document as input, and output language blocks with markups.

```
$ cat sample.txt
I don't speak English, or maybe I do. Je ne dis pas ce que je faisais. わたしは日本語を話せません。我不会说中文。Я не говорю по-русски.
```

You need the model pickl and labels to run the following. Download model.zip here(https://drive.google.com/open?id=1F7L0NvbbIpAvZY4wAGFLMrd3vD1ww6kh), and place the folder in the main directory.
```
$ python lang_detect.py sample.txt
I don't speak English, or maybe I do. ----> en
Je ne dis pas ce que je faisais. ----> fr
わたしは日本語を話せません. ----> ja
我不会说中文. ----> zh
Я не говорю по-русски. ----> ru
```

```
$ cat output.txt
<en> I don't speak English, or maybe I do. </en>
<fr> Je ne dis pas ce que je faisais. </fr>
<ja> わたしは日本語を話せません. </ja>
<zh> 我不会说中文. </zh>
<ru> Я не говорю по-русски. </ru>
```

---

## Data selection

Wikipedia is one of the most popular websites on earth with contributions from users around the globe. We can reasonably assume it reflects how we write in each language. Also, Wikipedia is written in more than 300 languages with labels, and relationship to the same articles in other languages.  At last, once we establish the pipeline, it is easy to obtain and add other languages to the software with wikidump(detail below).

---

## Data collection method

 I have obtained the data from Wikidump(https://dumps.wikimedia.org/), a public data dump of Wikipedia contents. And I will use WikiExtractor(https://github.com/attardi/wikiextractor) to extract the text da.


```
git clone https://github.com/attardi/wikiextractor.git
```

The subdirectories are named based on ISO_639-1(https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes). We can download other wiki dumps by changing parameters. This process could be automated with crawler as well.


```
wget -P wikiextractor/wikidump http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles.xml.bz2

wget -P wikiextractor/wikidump http://dumps.wikimedia.org/frwiki/latest/frwiki-latest-pages-articles.xml.bz2

wget -P wikiextractor/wikidump http://dumps.wikimedia.org/jawiki/latest/jawiki-latest-pages-articles.xml.bz2
```

---

## Approach

My approach is to make a sentence level language classifier so that it can deal with documents containing multiple languages, as well as short texts.  The software separates the document into sentences, and output each sentence with markups.

---

## Data extractin from Wikidump file

```
python wikiextractor/WikiExtractor.py -o data/en --no-templates --processes 8 wikiextractor/wikidump/enwiki-latest-pages-articles.xml.bz2

python wikiextractor/WikiExtractor.py -o data/fr --no-templates --processes 8 wikiextractor/wikidump/frwiki-latest-pages-articles.xml.bz2

python wikiextractor/WikiExtractor.py -o data/ja --no-templates --processes 8 wikiextractor/wikidump/jawiki-latest-pages-articles.xml.bz2
```

WikiExtractor.py parse XML and extract clean text from wiki dumps. 

---

## Data processing (Create data sets for machine learning models)

```
python process_wikipedia.py
```

WikiExtractor saves Wiki articles into the plan text separated with <doc> blocks. 
process_wikipedia.py do the following tasks
1. Split all the files in subdirectories into articles
2. Remove HTML tags
3. Replace Japanese punctuation for the tokeniser
4. Split into sentences with nltk library
5. Mark the language
6. Repeat 1-4 for all wiki language extractions.


---

## Training approach

The core approach is to use character features for classification. 

I kept special characters in the text, and I used character analyser from sklearn, TfidfVectorizer(https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html) to extract features on the character level. In general, languages have various special characters and prefixes, suffixes that are unique to the languages. Hence, I consider it to be the key feature for language identification. Also, close languages such as English and French share a significant amount of words. It is harder to distinguish by its word than characters. Moreover, word level classifier would require a larger amount of data, since there are far more combinations of words than characters. 


---

## Algorithm

Logistic regression is widely used binary classification model. It fits a logistic function to binary outcome. In the case of multi-class, we can use one vs all approach; create a classifier for each class vs other classes.  

sklearn - logistic regression 
https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html

---

## Evaluation

```
python train.py
```

### Number of samples

```
  lang   counts
0   en  1424904
1   fr  1285157
2   ja   935518
```

We have about 1 to 1.5 million samples for each class. And we do not have precisely a balanced class. We can use the F1 score as the measure rather than accuracy. Accuracy is highly dependent on the proportion or balance of class, and gives bias to larger classes.  We will use the F1 score to measure the performance.

F1 score = 2 * ( (precision * recall ) / precision + recall )

F1 score balances precision and recall. Precision measure whether predictions made by the model are correct. Recall measures whether everything that should be predicted, is predicted. The recall is also called sensitivity.  The F1 score is a robust measure to unbalanced class as well.


### F1 Score

```
             precision    recall  f1-score   support

         en       1.00      1.00      1.00    284683
         fr       1.00      1.00      1.00    257247
         ja       1.00      1.00      1.00    187186

avg / total       1.00      1.00      1.00    729116
```

The model shows all 1 in F1 score, which is maximum value it can get. Number of test samples for all classes are all around 200, 000.  On character level,  distinguishing the three language probably is trivial. When there is one acute accent, you can tell it is French since only French use acute accent among the three. Needless to mention Japanese
characters. Hence, although it works very well on the three languages, the performance may not be as good when we add other European and Asian languages.


F1 score
https://en.wikipedia.org/wiki/F1_score

---

### Followup - adding Russian and Chinese
```
lang  counts
0   en  759800
1   fr  679532
2   ja  553059
3   ru  472596
4   zh  755625
```

F1 score
```
precision    recall  f1-score   support

en       0.99      1.00      0.99    151960
fr       1.00      1.00      1.00    135552
ja       1.00      0.99      0.99    110599
ru       1.00      1.00      1.00     94621
zh       0.99      1.00      0.99    151391

avg / total       0.99      0.99      0.99    644123
```
I have added Russian and Chinese. The model still maintains the same high level of score. We can say that the model is robust even within the same group of languages. Please refer to Feature Analysis notebook for features leant by the model.

---

## Improvements upon deployment

1. load time

lang_detect.py loads pre-trained model, and the loading takes time. It is better to expose the model as a web API.

2. Punctuation of non-alphabetic languages

nltk.sent_tokenize() did not work well with Japanese punctuation, and I have replaced common Japanese punctuation with English ones during preprocessing.  Chinese will also require similar kind of processing, and probably many other languages as well, especially non-alphabetic languages. They will need to be tested before we can integrate into the standard data pipeline.


---
